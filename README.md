# FAIR-DS AP4.2 Demonstrator - Quality Assurance and Data Validation 

## What is this Repository?

This repository contains the .gitlab-ci.yml file which can be used to run
the Quality Assurance and Data Validation demonstrator on your own data. 

You can either fork this repository and add your own data or copy the 
gitlab-ci.yml and add it to an existing workflow. 

## What do I need to provide?

The demonstrator currently operates in one of two ways:

### Locally

Data files and schemas can be stored directly in this repository under
the "data" and "schemas" directories. The data must be in either "csv" 
or "parquet" format and the schemas must be in "JSON" format. These files
may be organized in their respective folders in any structure desired. 

### S3 Bucket

The Data and Schemas files may be located in a S3 bucket, provided that 
the repository has the keys needed to access that data. In order for the 
demonstrator to use these keys, you must add them to the the repository 
under `Settings -> CI/CD Settings -> Variables`. The demonstrator looks
for two specific variables at run time:
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY


## Can I select a specific version of the demonstrator?

The project maintains several versions of the demonstrator, labeled by
tags in the main repository. By default, we use the most recent version
of the published container. If you would like to use a specific version,
you can set the `VERSION: "latest"` to any valid container. A list of all
available containers can be found [here](https://git.rwth-aachen.de/fair-ds/ap-4-2-demonstrator/ap-4.2-data-validation-and-quality-assurance-demonstrator/container_registry/2585).
